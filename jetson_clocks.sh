#!/bin/sh

# turn on fan for safety
echo "Enabling fan for safety..."
if [ ! -w /sys/devices/pwm-fan/target_pwm ] ; then
	echo "Cannot set fan -- exiting..."
fi
echo 255 > /sys/devices/pwm-fan/target_pwm

nvpmodel -m 0

# online all CPUs - ignore errors for already-online units
echo "onlining CPUs: ignore errors..."
for i in 0 1 2 3 ; do
	echo 1 > /sys/devices/system/cpu/cpu${i}/online
done
echo "Online CPUs: `cat /sys/devices/system/cpu/online`"

# set CPUs to max freq
echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
cpumax=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies | awk '{print $NF}'`
echo "${cpumax}" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
for i in 0 1 2 3 ; do
	echo "CPU${i}: `cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq`"
done
