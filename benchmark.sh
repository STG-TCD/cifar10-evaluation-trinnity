#!/usr/bin/env bash

: ${CLEAN=0}

: ${ENABLE_PGO:=0}
: ${ENABLE_LTO:=0}
: ${OPTIMIZE_SIZE:=0}
: ${PRODUCE_ASSEMBLER:=0}

: ${BENCHMARK_TRINNITY:=1}
: ${BENCHMARK_CAFFE:=0}

: ${COMPUTE_STATS:=1}
: ${DISPLAY_VARIANCE:=0}

: ${CROSS_COMPILE:=0}
: ${CROSS_HOST:=ubuntu@stg-a57-devkit}
: ${CROSS_DIR:=/home/ubuntu}
: ${CROSS_CPU:=cortex-a57}

: ${MODEL_DIR:="../CIFAR10-DEPLOYMENT-MODELS"}
: ${ALGO_DESCRIPTOR_FILE:=triNNity-algorithms.csv}
: ${METHOD_DESCRIPTOR_FILE:=triNNity-methods.csv}

: ${OPTIMIZE:=0}
: ${HEURISTIC:=SUM2D_CHW}

: ${RUNS:=1500}
: ${CAFFE_RUNS:=1500}
: ${WINDOW=50}

MODELS=(LeNet CIFAR10 ResNet-20 SqueezeNet MobileNet AlexNet)
#(AlexNet AlexNet-ng GoogleNet NIN)

LTO_FLAGS=$([ "$ENABLE_LTO" == 1 ] && echo "-flto -fwhole-program")

CXX=g++
STRIP=strip
BLAS=libopenblas.so
FAST_FLAGS="-Ofast -pthread -march=native -mtune=native -Wall -Wno-error=coverage-mismatch --pedantic $LTO_FLAGS -fopenmp -faligned-new -fno-rtti -fno-exceptions -fno-stack-protector -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -DTRINNITY_NO_DEALLOCATE_LAYERS -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER -DTRINNITY_NO_ASSERTS -DTRINNITY_NO_EXCEPTIONS"
SMALL_FLAGS="-Os -pthread -march=native -mtune=native -Wall -Wno-error=coverage-mismatch --pedantic $LTO_FLAGS -fopenmp -faligned-new -fno-rtti -fno-exceptions -fno-stack-protector -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -DTRINNITY_NO_DEALLOCATE_LAYERS -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER -DTRINNITY_NO_ASSERTS -DTRINNITY_NO_EXCEPTIONS"
#DEBUG_FLAGS="-Ofast -march=native -mtune=native -Wall -Wno-error=coverage-mismatch --pedantic $LTO_FLAGS -fopenmp -faligned-new -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER"

CROSS_CXX=aarch64-linux-gnu-g++
CROSS_STRIP=aarch64-linux-gnu-strip
CROSS_BLAS=libopenblas.a
CROSS_FAST_FLAGS="-static -Ofast -pthread -mcpu=$CROSS_CPU -l:libgomp.a -l:libopenblas.a -Wall -Wno-error=coverage-mismatch --pedantic $LTO_FLAGS -fopenmp -faligned-new -fno-rtti -fno-exceptions -fno-stack-protector -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -DTRINNITY_NO_DEALLOCATE_LAYERS -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER -DTRINNITY_NO_ASSERTS -DTRINNITY_NO_EXCEPTIONS"
CROSS_SMALL_FLAGS="-static -Os -pthread -mcpu=$CROSS_CPU -l:libgomp.a -l:libopenblas.a -Wall -Wno-error=coverage-mismatch --pedantic $LTO_FLAGS -fopenmp -faligned-new -fno-rtti -fno-exceptions -fno-stack-protector -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -DTRINNITY_NO_DEALLOCATE_LAYERS -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER -DTRINNITY_NO_ASSERTS -DTRINNITY_NO_EXCEPTIONS"

THREADS=1
CORES=0
LAYER_COST_FILE=layers-singlethreaded.csv
TRANS_COST_FILE=transforms-singlethreaded.csv

#THREADS=2
#CORES=0,1
#LAYER_COST_FILE=layers-2threads.csv
#TRANS_COST_FILE=transforms-2threads.csv

#THREADS=4
#CORES=0,1,2,3
#LAYER_COST_FILE=layers-4threads.csv
#TRANS_COST_FILE=transforms-4threads.csv

for model in ${MODELS[*]}
do
  if [[ $BENCHMARK_TRINNITY != 0 ]]
  then
    echo; echo "+ Starting build of $model"

    PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend optimizer-info --model $MODEL_DIR/$model-deploy.prototxt --constraints-output=$MODEL_DIR/$model.constraints

    PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend optimizer-info --model $MODEL_DIR/$model-deploy.prototxt --layers-output=$MODEL_DIR/$model.layers

    PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend optimizer-info --model $MODEL_DIR/$model-deploy.prototxt --topology-output=$MODEL_DIR/$model.topology

    if [[ $OPTIMIZE == 1 ]]
    then
      echo; echo "+ Generating optimization query for $model"

      time triNNity-optimizer pbqp -t $MODEL_DIR/$model.topology \
                                   -s $MODEL_DIR/$model.constraints \
                                   -a $ALGO_DESCRIPTOR_FILE \
                                   -n $LAYER_COST_FILE \
                                   -e $TRANS_COST_FILE \
                                   > $MODEL_DIR/$model.pbqp

      echo; echo -n "+ Finished generating query, PBQP problem size (nodes, edges, bytes): "

      head -n2 $MODEL_DIR/$model.pbqp | tr '\n' ' ' | awk '{printf "%d, %d, ", $1, $2*$3}'; du -hs $MODEL_DIR/$model.pbqp | awk '{print $1}'

      echo; echo "+ Invoking solver for optimal $model"

      time pbqp_solve $MODEL_DIR/$model.pbqp h > $MODEL_DIR/$model.solution

      echo; echo "+ Generating code for optimized $model"
    else
      time triNNity-optimizer heuristic -t $MODEL_DIR/$model.topology \
                                        -a $ALGO_DESCRIPTOR_FILE \
                                        -n /dev/null \
                                        -x $HEURISTIC \
                                        > $MODEL_DIR/$model.solution

      echo; echo "+ Generating code for unoptimized $model"
    fi

    triNNity-optimizer instantiate -t $MODEL_DIR/$model.topology \
                                   -a $ALGO_DESCRIPTOR_FILE \
                                   -m $METHOD_DESCRIPTOR_FILE \
                                   -s $MODEL_DIR/$model.solution \
                                   -i TRINNITY \
                                   -l $MODEL_DIR/$model.layers \
                                   > $MODEL_DIR/$model.h

    PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend trinnity --model $MODEL_DIR/$model-deploy.prototxt --code-output=$MODEL_DIR/$model-impl.hpp

    # Create a harness CPP file to actually load and run the model
    cat << EOF > $MODEL_DIR/$model.cpp
#include <chrono>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <trinnity-compiler/runtimes/triNNity.hpp>
#include "$model.h"
#include "$model-impl.hpp"

int main(int argc, char **argv) {

  std::string weights_dir;
  std::string input_file;
  std::string output_file;

  bool benchmarking_mode = false;

  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <weights directory> <input> <output>" << std::endl;
    std::cerr << "Running in benchmarking mode since no input provided" << std::endl;
    benchmarking_mode = true;
  } else {
    weights_dir = std::string(argv[1]);
    input_file = std::string(argv[2]);
    output_file = std::string(argv[3]);

    std::cerr << "Loading weights from " << weights_dir << std::endl;
    std::cerr << "Loading input from " << input_file << std::endl;
    std::cerr << "Output to " << output_file << std::endl;
  }

  unsigned times[NO_OF_RUNS];

  Network<ACTIVATION_TYPE, WEIGHT_TYPE> net(weights_dir, output_file, input_file, benchmarking_mode);

  net.setup();

  net.load_input();

  auto t1 = std::chrono::high_resolution_clock::now();

  for (unsigned i = 0; i < NO_OF_RUNS; i++) {
    net.infer();
    auto t2 = std::chrono::high_resolution_clock::now();
    times[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count();
    t1 = t2;
  }

  for (unsigned i = 0; i < NO_OF_RUNS; i++) {
    std::cout << times[i] << std::endl;
  }

  net.save_output();

  //net.teardown();

  return 0;
}
EOF

    if [[ $CROSS_COMPILE == 0 ]]
    then
      if [[ $ENABLE_PGO == 0 ]]
      then
        if [[ $OPTIMIZE_SIZE != 0 ]]
        then
          FLAGS=$SMALL_FLAGS
        else
          FLAGS=$FAST_FLAGS
        fi

        # Build model for native arch, no PGO
        echo; echo "+ Compiling $model without PGO with $CXX using flags"

        echo "+ $FLAGS"

        if [[ $PRODUCE_ASSEMBLER == 0 ]]
        then
          time $CXX --std=c++17 $FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$BLAS -o $MODEL_DIR/$model.exe && $STRIP $MODEL_DIR/$model.exe
        else
          time $CXX --std=c++17 $FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$BLAS -S -o $MODEL_DIR/$model.s
        fi
      else
        if [[ $OPTIMIZE_SIZE != 0 ]]
        then
          FLAGS=$SMALL_FLAGS
        else
          FLAGS=$FAST_FLAGS
        fi

        # Build model for native arch, PGO
        echo; echo "+ Compiling $model in profiling mode with $CXX using flags"

        echo "+ $FLAGS"

        # Build the PGO sampler
        time $CXX -fprofile-generate=$model.profile --std=c++17 $FLAGS -DNO_OF_RUNS=5 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$BLAS -o $MODEL_DIR/$model.pgo-sampler

        echo; echo "+ Launching profiling run for $model"

        # Run the PGO sampler
        OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES ./$MODEL_DIR/$model.pgo-sampler | awk '{printf "%g ms\n", $1/1e6}'

        echo; echo "+ Compiling $model with $CXX using flags"

        echo "+ $FLAGS"

        if [[ $PRODUCE_ASSEMBLER == 0 ]]
        then
          time $CXX -fprofile-use=$model.profile --std=c++17 $FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$BLAS -o $MODEL_DIR/$model.exe && $STRIP $MODEL_DIR/$model.exe
        else
          time $CXX -fprofile-use=$model.profile --std=c++17 $FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$BLAS -S -o $MODEL_DIR/$model.s
        fi
      fi
    else
      if [[ $ENABLE_PGO == 0 ]]
      then
        if [[ $OPTIMIZE_SIZE != 0 ]]
        then
          CROSS_FLAGS=$CROSS_SMALL_FLAGS
        else
          CROSS_FLAGS=$CROSS_FAST_FLAGS
        fi

        # Build model for aarch64, no PGO
        echo; echo "+ Compiling $model with $CROSS_CXX using flags"

        echo "+ $CROSS_FLAGS"

        if [[ $PRODUCE_ASSEMBLER == 0 ]]
        then
          time $CROSS_CXX --std=c++17 $CROSS_FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$CROSS_BLAS -o $MODEL_DIR/$model.exe && $CROSS_STRIP $MODEL_DIR/$model.exe
        else
          time $CROSS_CXX --std=c++17 $CROSS_FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$CROSS_BLAS -S -o $MODEL_DIR/$model.s
        fi
      else
        if [[ $OPTIMIZE_SIZE != 0 ]]
        then
          CROSS_FLAGS=$CROSS_SMALL_FLAGS
        else
          CROSS_FLAGS=$CROSS_FAST_FLAGS
        fi

        # Build model for aarch64, PGO
        echo; echo "+ Compiling $model in profiling mode with $CROSS_CXX using flags"

        echo "+ $CROSS_FLAGS"

        # Build the PGO sampler
        time $CROSS_CXX -fprofile-generate=$model.profile --std=c++17 $CROSS_FLAGS -DNO_OF_RUNS=5 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$CROSS_BLAS -o $MODEL_DIR/$model.pgo-sampler

        echo; echo "+ Launching profiling run for $model"

        # Push the PGO sampler
        scp $MODEL_DIR/$model.pgo-sampler $CROSS_HOST:$CROSS_DIR

        # Run the PGO sampler
        ssh $CROSS_HOST "cd $CROSS_DIR && OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES ./$model.pgo-sampler | awk '{printf \"%g ms\n\", \$1/1e6}'"

        # Pull the profile back
        scp -r $CROSS_HOST:$CROSS_DIR/$model.profile .

        echo; echo "+ Compiling $model with $CXX using flags"

        echo "+ $CROSS_FLAGS"

        if [[ $PRODUCE_ASSEMBLER == 0 ]]
        then
          time $CROSS_CXX -fprofile-use=$model.profile --std=c++17 $CROSS_FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$CROSS_BLAS -o $MODEL_DIR/$model.exe && $CROSS_STRIP $MODEL_DIR/$model.exe
        else
          time $CROSS_CXX -fprofile-use=$model.profile --std=c++17 $CROSS_FLAGS -DNO_OF_RUNS=$RUNS -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float $MODEL_DIR/$model.cpp -l:$CROSS_BLAS -S -o $MODEL_DIR/$model.s
        fi
      fi
    fi

    echo; echo "+ Converting weights for $model"

    mkdir -p $MODEL_DIR/$model.parameters

    PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --model $MODEL_DIR/$model-deploy.prototxt --weights $MODEL_DIR/$model.bestmodel --data-output=$MODEL_DIR/$model.parameters

    echo; echo "+ Finished building $model"

    if [[ $PRODUCE_ASSEMBLER == 0 ]]
    then
      echo; echo "+ Benchmarking $model using $RUNS inferences"

      if [[ $CROSS_COMPILE == 0 ]]
      then
        OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES ./$MODEL_DIR/$model.exe | awk '{printf "%g ms\n", $1/1e6}' > $MODEL_DIR/$model.dat
      else
        scp $MODEL_DIR/$model.exe $CROSS_HOST:$CROSS_DIR
        ssh $CROSS_HOST "cd $CROSS_DIR && OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES ./$model.exe | awk '{printf \"%g ms\n\", \$1/1e6}'" > $MODEL_DIR/$model.dat
      fi
    fi
  fi

  if [[ $BENCHMARK_CAFFE != 0 ]]
  then
    echo; echo "+ Benchmarking $model using Caffe with $CAFFE_RUNS inferences"

    if [[ $CROSS_COMPILE == 0 ]]
    then
      OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES caffe time -model=$MODEL_DIR/$model-deploy.prototxt  -weights=$MODEL_DIR/$model.bestmodel -iterations=$CAFFE_RUNS 2>&1 | grep "Average Forward pass" | sed 's/ms./ms/g' | awk '{printf "%g %s\n", $8, $9}'
    else
      # Push model and weights
      scp $MODEL_DIR/$model-deploy.prototxt $CROSS_HOST:$CROSS_DIR
      scp $MODEL_DIR/$model.bestmodel $CROSS_HOST:$CROSS_DIR
      ssh $CROSS_HOST "cd $CROSS_DIR && OPENBLAS_NUM_THREADS=$THREADS OMP_NUM_THREADS=$THREADS taskset -c $CORES caffe time -model=$model-deploy.prototxt -iterations=$CAFFE_RUNS 2>&1 | grep \"Average Forward pass\" | sed 's/ms./ms/g' | awk '{printf \"%g %s\\n\", \$8, \$9}'"
    fi
  fi

  if [[ $PRODUCE_ASSEMBLER == 0 ]]
  then
    if [[ $COMPUTE_STATS != 0 ]]
    then
      echo; echo "+ Statistics for model $model"
      # Compute statistics about the benchmark run

      if [[ $DISPLAY_VARIANCE != 0 ]]
      then
          # Checking for the steady-state: look at the behaviour of the windowed variance across the samples
          echo -n "+ Checking for steady-state (window=$WINDOW inferences): "
          python -c "import pandas;[print(x) for x in (pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]).rolling(window=$WINDOW).var()[0]) if abs(x) > 0]" | gnuplot -e "set title \"$model\";set ylabel \"variance (ms)\";set xrange [-100:$(($RUNS+100))];set yrange[0:];plot '-' w linespoints title 'inference time variance'" -persist
      fi

      # First: peak throughput, inferences per second
      echo -n "+ Peak Maximum Throughput (window=$WINDOW inferences): "
      python -c "import pandas;print(\"{} ips\".format(round(((1000)/((1/$WINDOW)*(pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]).rolling(window=$WINDOW).sum().min()[0]))), 2)))"

      # Second: steady-state throughput, inferences per second
      echo -n "+ Steady-State Throughput (window=$WINDOW inferences): "
      python -c "import pandas;df=pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]);vars=[x for x in list(df.rolling(window=$WINDOW).var()[0]) if x > 0];loc=vars.index(min(vars));print(\"{} ips @ T+{} -- T+{}\".format(round(((1000)/(df.loc[loc:loc+($WINDOW-1)].mean()[0])), 2), loc, loc+($WINDOW-1)))"

      # Third: latency
      echo -n "+ Steady-State Latency (min) (window=$WINDOW inferences): "
      python -c "import pandas;df=pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]);vars=[x for x in list(df.rolling(window=$WINDOW).var()[0]) if x > 0];loc=vars.index(min(vars));print(\"{} ms @ T+{} -- T+{}\".format(round(df.loc[loc:loc+($WINDOW-1)].min()[0], 2), loc, loc+($WINDOW-1)))"

      echo -n "+ Steady-State Latency (max) (window=$WINDOW inferences): "
      python -c "import pandas;df=pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]);vars=[x for x in list(df.rolling(window=$WINDOW).var()[0]) if x > 0];loc=vars.index(min(vars));print(\"{} ms @ T+{} -- T+{}\".format(round(df.loc[loc:loc+($WINDOW-1)].max()[0], 2), loc, loc+($WINDOW-1)))"

      echo -n "+ Steady-State Latency (mean) (window=$WINDOW inferences): "
      python -c "import pandas;df=pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]);vars=[x for x in list(df.rolling(window=$WINDOW).var()[0]) if x > 0];loc=vars.index(min(vars));print(\"{} ms +/- {} ms @ T+{} -- T+{}\".format(round(df.loc[loc:loc+($WINDOW-1)].mean()[0], 2), round(df.loc[loc:loc+($WINDOW-1)].std()[0], 3),loc, loc+($WINDOW-1)))"

      echo -n "+ Median Latency (window=ALL inferences): "
      python -c "import pandas;print(\"{} ms\".format(round(pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]).median()[0], 2)))"

      echo -n "+ Median Throughput (window=ALL inferences): "
      python -c "import pandas;print(\"{} ips\".format(round(1000/pandas.read_csv(\"$MODEL_DIR/$model.dat\", sep=\" \", header=None, usecols=[0]).median()[0]), 2))"
    fi
  fi

  if [[ $CLEAN != 0 ]]
  then
    # To clean up the model directory
    find $MODEL_DIR -maxdepth 1 -type f \( ! -iname '*.bestmodel' \) -and \( ! -iname '*.prototxt' \) | xargs rm -f
  fi

  # To make a CSV of triNNity results
  #cat singlethreaded.log | grep -A5 'Statistics for model' | sed 's/--//g' | sed 's/+ //g' | sed 's/:/,/g' | sed 's/ +\/-/,/g' > results-trinnity.csv

  # To make a CSV of Caffe results
  #cat singlethreaded.log | grep -A4 'Benchmarking' | grep -A1 '.bestmodel' | awk '{print $1}' | tr '\n' ',' | sed 's/--,/\n/g' | sed 's/,$//g' > results-caffe.csv

done
